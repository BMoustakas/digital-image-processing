function color = bilinear(image,point,height,width)
sum = uint32(zeros(1,1,3));
neighbourPixelsNumber = uint32(0);
    if (point(2)-1>0)
        sum = sum + uint32(image(point(1),point(2)-1,:));
        neighbourPixelsNumber = neighbourPixelsNumber +1;
    end
    if (point(1)-1>0)
        sum = sum + uint32(image(point(1)-1,point(2),:));
        neighbourPixelsNumber = neighbourPixelsNumber +1;
    end
    if (point(2)+1<=width)
       sum = sum + uint32(image(point(1),point(2)+1,:));
       neighbourPixelsNumber = neighbourPixelsNumber +1;
    end
    if (point(1)+1<=height)
        sum = sum + uint32(image(point(1)+1,point(2),:));
        neighbourPixelsNumber = neighbourPixelsNumber +1;
    end
    color = uint8(sum/neighbourPixelsNumber);
end