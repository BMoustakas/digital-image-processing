function newImage = myImgRotation(image,theta)
%MYIMGROTATION Summary of this function goes here
%   Detailed explanation goes here
R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];
invR = inv(R);
[height,width,~] = size(image);

corners = zeros(4,2);
corners(1,:) = R*[1;1];
corners(2,:) = R*[height;1];
corners(3,:) = R*[1;width];
corners(4,:) = R*[height;width];

minHeight = min(floor(corners(:,1)));
maxHeight = max(ceil(corners(:,1)));
minWidth = min(floor(corners(:,2)));
maxWidth = max(ceil(corners(:,2)));

newImage = zeros(maxHeight-minHeight, maxWidth - minWidth,3);

for i = minHeight:maxHeight
    for j = minWidth:maxWidth
        newPoint = invR*[i;j];
        newPoint = floor(newPoint);
        if (newPoint(1)<=height && newPoint(2)<=width && newPoint(1)>0 &&newPoint(2)>0)
            color = bilinear(image,newPoint,height,width);
            newImage(i+abs(minHeight)+1,j+abs(minWidth)+1,:) = color;
        else
             newImage(i+abs(minHeight)+1,j+abs(minWidth)+1,:) = uint8(ones(1,1,3));
        end
    end
end

newImage = uint8(newImage);

end



