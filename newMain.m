clear all;
close all;

image1 = imread('TestIm1.png');
image2 = imread('TestIm2.png');

angle = 0;
newImage1 = myImgRotation(image1,angle);
newImage2 = myImgRotation(image2,angle);

grayImage1 = rgb2gray(newImage1);
grayImage2 = rgb2gray(newImage2);

grayImageDouble1 = double(grayImage1);
grayImageDouble2 = double(grayImage2);

[Corners1,indices1] = allCorners(grayImageDouble1);
[Corners2,indices2] = allCorners(grayImageDouble2);

Corners1 = uint8(Corners1);
Corners2 = uint8(Corners2);

figure()
imshow(Corners1)
figure()
imshow(Corners2)


descriptorValue1 = zeros(length(indices1),16);
for i = 1:length(indices1)
    point = [indices1(1,i),indices1(2,i)];
    d = myLocalDescriptor(grayImage1,point,5,1,20,20);
    if isempty(d)
        descriptorValue1(i,:)  = zeros(1,16);
    else
        descriptorValue1(i,:) = d;
    end
end
descriptorValue2 = zeros(length(indices2),16);
for i = 1:length(indices2)
    point = [indices2(1,i),indices2(2,i)];
    d = myLocalDescriptor(grayImage2,point,5,1,20,20);
    if isempty(d)
        descriptorValue2(i,:) = zeros(1,16);
    else
        descriptorValue2(i,:) = d;
    end
end

matchedValues = 200*ones(3,1);
matchedIndices1 = zeros(3,2);
matchedIndices2 = zeros(3,2);

for i = 1:length(descriptorValue1)
    if mean(descriptorValue1(i,:))>0
        for j = 1:length(descriptorValue2)
            if mean(descriptorValue2(j,:))>0
                mse = immse(descriptorValue1(i,:),descriptorValue2(j,:));
                if mse<matchedValues(1)
                    matchedValues(3) = matchedValues(2);
                    matchedValues(2) = matchedValues(1);
                    matchedValues(1) =  mse;
                    matchedIndices1(3,:) = matchedIndices1(2,:);
                    matchedIndices1(2,:) = matchedIndices1(1,:);
                    matchedIndices1(1,:) = indices1(:,i);
                    matchedIndices2(3,:) = matchedIndices2(2,:);
                    matchedIndices2(2,:) = matchedIndices2(1,:);
                    matchedIndices2(1,:) = indices2(:,j);
                elseif  mse<matchedValues(2)
                    matchedValues(3) = matchedValues(2);
                    matchedValues(2) =   mse;
                    matchedIndices1(3,:) = matchedIndices1(2,:);
                    matchedIndices1(2,:) = indices1(:,i);
                    matchedIndices2(3,:) = matchedIndices2(2,:);
                    matchedIndices2(2,:) = indices2(:,j);
                elseif  mse<matchedValues(3)
                    matchedValues(3) = mse;
                    matchedIndices1(3,:) = indices1(:,i);
                    matchedIndices2(3,:) = indices2(:,j);
                end
            end
        end
    end
end

[height,width] = size(grayImage2);
im = zeros(height,width);
for i = 1:3
    im(matchedIndices2(i,1),matchedIndices2(i,2)) = 255;
end
im = uint8(im);
figure()
imshow(im)
[height,width] = size(grayImage1);
im = zeros(height,width);
for i = 1:3
    im(matchedIndices1(i,1),matchedIndices1(i,2)) = 255;
end
im = uint8(im);
figure()
imshow(im)


