function c = isCorner(image,point,k,Rthres)
%ISCORNER Summary of this function goes here
%   Detailed explanation goes here

    global Ix2;
    global Iy2;
    global Ixy;

    y = point(1);
    x = point(2);
    u1 = 2;
    u2 = 2;
    [height,width] = size(image);
    

   
    Ix2sum = 0;
    Iy2sum = 0;
    Ixysum = 0;
    
    for i = 1:u1
        for j = 1:u2
            if (y+i<=height && x+j<=width)
                Ix2sum = Ix2sum + Ix2(y+i,x+j);
                Iy2sum = Iy2sum + Iy2(y+i,x+j);
                Ixysum = Ixysum + Ixy(y+i,x+j);
            end
        end
    end
    
    A = [Ix2sum, Ixysum; Ixysum, Iy2sum];
    H = A/(u1*u2);
    R(y,x) = det(H) - k*trace(H)^2;
    
    if R(y,x) > Rthres
        c = 255;
    else
        c = 0;
    end
    
end

