clear all;
close all;

image = imread('TestIm1.png');
image2 = imread('TestIm2.png');

angle = 0;
newImage = myImgRotation(image,angle);
newImage2 = myImgRotation(image2,angle);
% newImage = imrotate(image,angle,'bilinear');
% imshow(newImage);

grayImage = rgb2gray(newImage);

grayImage2 = rgb2gray(newImage2);
% rhom = 4;
% rhostep = 1;
% rhoM = 20;
% N = 36;
% point = [523,546];

% [d,newImage] = myLocalDescriptor(grayImage,point,rhom,rhostep,rhoM,N);
% d = myLocalDescriptorUpgrade(newImage,point,rhom,rhostep,rhoM,N);


% corners=detectHarrisFeatures(grayImage);
% figure
% hold on
% imshow(grayImage);
% plot(corners);
% hold off

[newHeight,newWidth] = size(grayImage);
grayImageDouble = double(grayImage);

Corners = allCorners(grayImageDouble);

grayImageDouble2 = double(grayImage2);

Corners2 = allCorners(grayImageDouble2);

% Ix = zeros(newHeight,newWidth);
% for i=1:newHeight
%     Ix(i,:) = gradient(grayImageDouble(i,:));
% end
% Iy = zeros(newHeight,newWidth);
% for i=1:newWidth
%     Iy(:,i) = gradient(grayImageDouble(:,i));
% end
% 
% global Ix2; Ix2 = Ix.^2;
% global Iy2; Iy2 = Iy.^2;
% global Ixy; Ixy = Ix.*Iy;
% R = zeros(newHeight,newWidth);
% Corners = zeros(newHeight,newWidth);
% 
% for y = 1:newHeight
%     for x = 1:newWidth
%         point = [y,x];
%         Corners(y,x) = isCorner(grayImageDouble,point,k,Rthres);
%     end
% end

tform = estimateGeometricTransform2D(Corners,Corners2,'rigid');

Ir = imwarp(image2,tform);
imshow(Ir);

% Corners = uint8(Corners);
% imshow(Corners);

% newImage = uint8(newImage);
% figure()
% imshow(newImage);
% figure()
% imshow(grayImage);






