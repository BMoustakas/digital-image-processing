% hom1 = cart2hom(matchedIndices1);
% hom2 = cart2hom(matchedIndices2);
% 
% M = hom1*inv(hom2)


theta1 = atand((matchedIndices1(1,1)-matchedIndices1(2,1))/(matchedIndices1(1,2)-matchedIndices1(2,2)));
theta2 = atand((matchedIndices2(1,1)-matchedIndices2(2,1))/(matchedIndices2(1,2)-matchedIndices2(2,2)));


[height3,width3,~] = size(newImage2);

angle = theta2-theta1;
image2Final = myImgRotation(newImage2,angle);

[height1,width1,~] = size(newImage1);
[height2,width2,~] = size(image2Final);

R = [cosd(angle) -sind(angle); sind(angle) cosd(angle)];

corners = zeros(4,2);
corners(1,:) = R*[1;1];
corners(2,:) = R*[height3;1];
corners(3,:) = R*[1;width3];
corners(4,:) = R*[height3;width3];

minHeight = min(floor(corners(:,1)));
maxHeight = max(ceil(corners(:,1)));
minWidth = min(floor(corners(:,2)));
maxWidth = max(ceil(corners(:,2)));

refPoint = round(R*matchedIndices2(1,:)'); 
refPoint = [refPoint(1)+abs(minHeight)+1,refPoint(2)+abs(minWidth)+1];
y_offset = refPoint(1) - matchedIndices1(1,1);
x_offset = refPoint(2) - matchedIndices1(1,2); 

finalImage = zeros(matchedIndices1(1,1)+(height2-refPoint(1)), matchedIndices1(1,2)+(width2-refPoint(2)),3); 

[height,width,~] = size(finalImage);

for y = 1:height
    for x = 1:width
        if (y<=height1)
            if (x<=width1)
                finalImage(y,x,:) = newImage1(y,x,:);
            else
                if (y<=height-height2)
                    finalImage(y,x,:) = zeros(3,1);
                else
                    finalImage(y,x,:) = image2Final(y-(height-height2),x-(width-width2),:);
                end
            end
        else
            if (x>width-width2)
                finalImage(y,x,:) = image2Final(y-(height-height2),x-(width-width2),:);
            else
                finalImage(y,x,:) = zeros(3,1);
            end
        end
        
    end
end





im = uint8(finalImage);
figure()
imshow(im)
