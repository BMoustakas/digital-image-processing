function d= myLocalDescriptorUpgrade(image,point,rhom,rhostep,rhoM,N)
%MYLOCALDESCRIPTORUPGRADE Summary of this function goes here
%   Detailed explanation goes here
[height,width,~] = size(image);
% newImage = zeros(height,width,3);
angleStep = 360/N;
numberOfCycles = length(rhom:rhostep:rhoM);
d = zeros(numberOfCycles,3);

for r = rhom:rhostep:rhoM
    xr = zeros(N,3);
    for n = 1:N
        pointAngle = n*angleStep;
        newPoint = [point(1)+r*sind(pointAngle), point(2)+r*cosd(pointAngle)];
        newPoint = round(newPoint);
        if (newPoint(1)>1 && newPoint(2)>1 && newPoint(1)<=height && newPoint(2)<=width)
            color = bilinear(image,newPoint,height,width);
            xr(n,:) = mean(color);
        else
            d = [];
            return;
        end
%         newImage(newPoint(1),newPoint(2),:) = color;
    end
    for i = 1:3
        d(r,i) = mean(xr(:,i));
    end
end
end



