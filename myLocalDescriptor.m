function d = myLocalDescriptor(image,point,rhom,rhostep,rhoM,N)
%MYLOCALDESCRIPTOR Summary of this function goes here
%   Detailed explanation goes here
[height,width,~] = size(image);
% newImage = zeros(height,width,3);
angleStep = 360/N;
numberOfCycles = length(rhom:rhostep:rhoM);
d = zeros(numberOfCycles,1);

for r = rhom:rhostep:rhoM
    xr = zeros(N,1);
    for n = 1:N
        pointAngle = n*angleStep;
        newPoint = [point(1)+r*sind(pointAngle), point(2)+r*cosd(pointAngle)];
        newPoint = floor(newPoint);
        if (newPoint(1)>1 && newPoint(2)>1 && newPoint(1)<=height && newPoint(2)<=width)
            color = bilinear(image,newPoint,height,width);
            xr(n) = mean(color);
        else
            d = [];
            return;
        end
%         newImage(newPoint(1),newPoint(2),:) = color;
    end
    d(r-rhom+1) = mean(xr);
end
end

