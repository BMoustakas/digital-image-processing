function [Corners,indices] = allCorners(image)
%ALLCORNERS Summary of this function goes here
%   Detailed explanation goes here


    [height,width] = size(image);
    Corners = zeros(height,width);
    k = 0.13;
    Rthres = 300000000;
    indices = [];

    Ix = zeros(height,width);
    for i=1:height
        Ix(i,:) = gradient(image(i,:));
    end
    Iy = zeros(height,width);
    for i=1:width
        Iy(:,i) = gradient(image(:,i));
    end

    Ix2 = Ix.^2;
    Iy2 = Iy.^2;
    Ixy = Ix.*Iy;
    
    m = 5;

    for y = 1:height
        for x = 1:width    
            A = 0;
            B = 0;
            C = 0;
            for i = 1:m
                if (x+m<=width && y+i<=height)
                    A = A + sum(conv(gausswin(m),Ix2(y+i,x:x+m)));
                    B = B + sum(conv(gausswin(m),Iy2(y+i,x:x+m)));
                    C = C + sum(conv(gausswin(m),Ixy(y+i,x:x+m)));
                end
            end
            H = [A C; C B];
            R = det(H) - k*trace(H)^2;
            if R > Rthres
                Corners(y,x) = 255;
                point = [y;x];
                indices = [indices,point];
            end
        end
    end
    
    

end

